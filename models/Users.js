import User from './User';

class Users {
  constructor() {
    this.users = [];
  }

  addUser(name) {
    const user = new User(name);
    this.users.push(user);
    return user;
  }

  getUserById(id) {
    return this.users.find((e) => e.id === id);
  }

  getUserByName(name) {
    return this.users.find((e) => e.name === name);
  }

  deleteUserById(id) {
    const idx = this.users.findIndex((e) => e.id === id);
    this.users.splice(idx, 1);
    return id;
  }
}

export default Users;
