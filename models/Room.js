import hat from 'hat';

class Room {
  constructor(name) {
    this.id = hat();
    this.name = name;
    this.users = [];
  }

  get countUser() {
    return this.users.length;
  }

  getUserNames() {
    return this.users.map((user) => user.name);
  }

  getUserSortNames() {
    return this.getWinners().map((user) => user.name);
  }

  addUser(user) {
    user.clear();
    this.users.push(user);
    return user;
  }

  isExistUser(user) {
    return this.users.find((u) => u.id === user.id);
  }

  deleteUser(user) {
    const id = this.users.findIndex((e) => e.id === user.id);
    if (id === -1) return null;
    this.users.splice(id, 1);
    user.clear();
    return user;
  }

  toggleReadyUser(user) {
    this.users = this.users.map((us) => {
      if (us.id === user.id) {
        us.isReady = !us.isReady;
      }
      return us;
    });
  }

  changeCompletedUser(user, completed) {
    this.users = this.users.map((us) => {
      if (us.id === user.id) {
        us.completed = completed;
        us.completedDate = completed === 100 ? new Date() : null;
      }
      return us;
    });
  }

  isReady() {
    return this.users.every((e) => e.isReady);
  }

  isCompleted() {
    return this.users.every((e) => e.completed === 100);
  }

  isEmpty() {
    return this.users.length === 0;
  }

  getWinners() {
    const completedUsers = this.users.filter((e) => e.completed === 100);
    const otherUsers = this.users.filter((e) => e.completed !== 100);
    completedUsers.sort((a, b) => a.completedDate - b.completedDate);
    otherUsers.sort((a, b) => b.completed - a.completed);
    const sortUsers = completedUsers.concat(otherUsers);
    return sortUsers.map(({ name, completed }) => ({ name, completed }));
  }

  clearGame() {
    this.users = this.users.map((us) => {
      return {
        ...us,
        isReady: false,
        completed: 0,
        completedDate: null,
      };
    });
  }
}

export default Room;
