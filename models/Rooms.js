import Room from './Room';

class Rooms {
  constructor(props) {
    this.rooms = [];
  }

  addRoom(name) {
    const room = new Room(name);
    this.rooms.push(room);
    return room;
  }

  getRooms() {
    return this.rooms.map(({ id, name, countUser }) => ({
      id,
      name,
      countUser,
    }));
  }

  getFilteredRoom(maxCountUsers) {
    const filteredRoom = this.rooms.filter((e) => !e.isReady() && e.users.length < maxCountUsers);
    return filteredRoom.map(({ id, name, countUser }) => ({
      id,
      name,
      countUser,
    }));
  }

  getRoomById(id) {
    return this.rooms.find((e) => e.id === id);
  }

  getRoomByName(name) {
    return this.rooms.find((e) => e.name === name);
  }

  deleteRoomById(id) {
    const idx = this.rooms.findIndex((e) => e.id === id);
    this.rooms.splice(idx, 1);
    return id;
  }

  addUserToRoom(roomId, user) {
    this.rooms = this.rooms.map((room) => {
      if (room.id === roomId) {
        room.addUser(user);
      }
      return room;
    });
    return this.getRoomById(roomId);
  }

  deleteUserFromRoom(roomId, user) {
    this.rooms = this.rooms.map((room) => {
      if (room.id === roomId) {
        room.deleteUser(user);
      }
      return room;
    });

    return this.getRoomById(roomId);
  }

  deleteUserFromAllRoom(user) {
    const room = this.rooms.find((rm) => rm.isExistUser(user));
    return room ? this.deleteUserFromRoom(room.id, user) : room;
  }

  checkIsEmptyRoom(roomId) {
    return this.getRoomById(roomId).isEmpty();
  }

  getEmptyRooms() {
    return this.rooms.filter((room) => room.isEmpty());
  }

  deleteEmptyRoom(roomId) {
    if (this.checkIsEmptyRoom(roomId)) {
      this.deleteRoomById(roomId);
      return true;
    }
    return false;
  }

  deleteEmptyRoom() {
    this.rooms = this.rooms.filter((room) => room.countUser !== 0);
  }

  toggleReadyUser(roomId, user) {
    this.rooms = this.rooms.map((room) => {
      if (room.id === roomId) {
        room.toggleReadyUser(user);
      }
      return room;
    });
    return this.getRoomById(roomId);
  }

  changeCompletedUser(roomId, user, completed) {
    this.rooms = this.rooms.map((room) => {
      if (room.id === roomId) {
        room.changeCompletedUser(user, completed);
      }
      return room;
    });
    return this.getRoomById(roomId);
  }
}

export default Rooms;
