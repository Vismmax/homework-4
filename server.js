import express from "express";
import http from "http";
import socketIO from "socket.io";
import socketHandler from "./socket";
import routes from "./routes";
import { STATIC_PATH, PORT } from "./config";
import cors from "cors";

const app = express();
const httpServer = http.Server(app);
const io = socketIO(httpServer);

app.use(
  cors({
    credentials: true,
    origin: ["http://localhost:3000"],
    optionsSuccessStatus: 200,
  })
);

app.use(express.static(STATIC_PATH));
routes(app);

app.get("*", (req, res) => {
  res.redirect("/app");
});

socketHandler(io);

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});
