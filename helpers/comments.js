class Comments {
  static hello(name) {
    return `Привет ${name}.`;
  }

  static join(name) {
    return `К нам присоединился ${name}.`;
  }

  static leave(name) {
    return `Гоночную трассу покинул ${name}.`;
  }

  static ready(name) {
    return `${name} готов.`;
  }

  static waituser() {
    return 'Ждем еще гонщиков.';
  }

  static introduce() {
    return `Всем привет! Рада приветствовать вас на этом знаменательном событии, такие гонки бывают только раз в истории. Меня зовут Дина, и сегодня именно я буду следить вместе с вами за происходящим.`;
  }

  static go(names) {
    return `Итак в первом заезде принимает участие ${names.length} игроков. Их имена всем знакомы, так как это известнейшие люди в мире гонок.`;
  }

  static welcome(names) {
    return `Поприветствуем их ${names.join(', ')}`;
  }

  static every(names) {
    const endStr =
      names.length > 2
        ? `, а вот отстает от всех ${names[names.length - 1]}. Но все еще может кардинально измениться`
        : '';
    return `Лидирующие позиции сейчас занимает ${names[0]}, его практически догоняет ${names[1]}${endStr}.`;
  }

  static lastlap(names) {
    return `Итак мы уже почти у финишной прямой и готовится пересечь ее гонщик ${names[0]}, но в спину ему дышит ${names[1]}, посмотрим, какой же будет финал.`;
  }

  static finish(name) {
    return `Финишную прямую пересек ${name}.`;
  }

  static end(names) {
    const endStr = names.length > 2 ? `, ну а третье ${names[3]}` : '';
    return `И вот завершился очередной этап гонок и мы имеем совершенно неожиданный результат. Первое место получает ${names[0]}, второе ${names[1]}${endStr}.`;
  }
}

export default Comments;
