import { texts, jokes } from '../data';

export const getTextById = (id) => {
  return texts[id];
};

export const getRandomTextId = () => {
  return Math.floor(Math.random() * texts.length);
};

export const getRandomJoke = () => {
  const jokeId = Math.floor(Math.random() * jokes.length);
  return jokes[jokeId];
};
