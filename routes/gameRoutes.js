import { Router } from 'express';
import { getTextById } from '../helpers/dataApi';

const router = Router();

router.get('/texts/:id', (req, res) => {
  const text = getTextById(req.params.id);
  res.send({ text });
});

export default router;
