import gameRoutes from './gameRoutes';
import appRoutes from './appRoutes';

export default (app) => {
  app.use('/game', gameRoutes);
  app.use('/app', appRoutes);
};
