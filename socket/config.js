export const MAXIMUM_USERS_FOR_ONE_ROOM = 5;
export const SECONDS_TIMER_BEFORE_START_GAME = 10;
export const SECONDS_FOR_GAME = 60;
export const INTERVAL_INFO_MESSAGE = 30;
export const INTERVAL_JOKES = 20;
export const PERCENT_LAST_LAP = 80;
