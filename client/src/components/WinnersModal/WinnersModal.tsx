import React from 'react';
import { Button, Modal, List, Header } from 'semantic-ui-react';

type Winner = {
  name: string;
  completed: number;
};

type Props = {
  winners: {
    show: boolean;
    data: Winner[];
  };
  close: () => void;
};

function WinnersModal({ winners, close }: Props) {
  const { show, data } = winners;
  const list = data.map((e, id) => (
    <List.Item key={e.name}>
      <List.Content>
        <List.Header>Daniel Louise</List.Header> (completed {e.completed}%)
      </List.Content>
    </List.Item>
  ));
  return (
    <Modal size="tiny" open={show} onClose={close}>
      <Modal.Header>List Winners</Modal.Header>
      <Modal.Content>
        <List ordered size="huge">
          {list}
        </List>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={close}>Ok</Button>
      </Modal.Actions>
    </Modal>
  );
}

export default WinnersModal;
