import React from 'react';
import './CommentMessage.css';

type Props = {
  message: string;
  img: string;
};

function CommentMessage({ message, img }: Props): JSX.Element {
  return (
    <div className="cm-container">
      <img className="cm-commentator" src={img} alt="" />
      <div className="cm-message">{message}</div>
    </div>
  );
}

export default CommentMessage;
