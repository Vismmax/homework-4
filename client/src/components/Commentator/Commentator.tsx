import React, { useEffect } from 'react';
import { toast } from 'react-toastify';
import CommentMessage from '../CommentMessage/CommentMessage';
import commentatorImg from '../../img/commentator.svg';
import helloImg from '../../img/hello_sd.gif';

type Props = {
  socket: SocketIOClient.Socket;
};

function Commentator({ socket }: Props): JSX.Element {
  useEffect(() => {
    socket.on('HELLO_MESSAGE', (message: string) => {
      toast(<CommentMessage message={message} img={helloImg} />);
    });
    socket.on('COMMENTATOR_MESSAGE', (message: string) => {
      toast(<CommentMessage message={message} img={commentatorImg} />);
    });
  }, []);

  return <></>;
}

export default Commentator;
