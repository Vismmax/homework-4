import React from 'react';
import { Header, Button, Segment, Card } from 'semantic-ui-react';
import './ListRooms.css';
import CardRoom, { TypeRoom } from '../CardRoom/CardRoom';

type Props = {
  rooms: TypeRoom[];
  createRoom: (name: string) => void;
  joinRoom: (id: string) => void;
};

function ListRooms({ rooms, createRoom, joinRoom }: Props): JSX.Element {
  const newRoom = () => {
    const nameRoom = prompt('Name new room');
    if (nameRoom) {
      createRoom(nameRoom);
    }
  };

  const join = (id: string) => joinRoom(id);

  const cards = rooms.map((room: TypeRoom) => <CardRoom key={room.id} room={room} joinRoom={join} />);

  return (
    <Segment basic size="massive" className="rooms-page full-screen">
      <Segment basic>
        <Header as="h1">Join Room Or Create New</Header>
        <Button size="massive" className="button-create-room" onClick={newRoom}>
          Create Room
        </Button>
      </Segment>
      <Segment basic size="large">
        <Card.Group basic id="cards-room" className="cards-room">
          {cards}
        </Card.Group>
      </Segment>
    </Segment>
  );
}

export default ListRooms;
