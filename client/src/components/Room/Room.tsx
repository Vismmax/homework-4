import React from 'react';
import { Header, Button, Segment, Icon } from 'semantic-ui-react';
import './Room.css';
import UserInfo from '../UserInfo/UserInfo';
import { TypeUser, TypeRoom, TextEditor, StatusGame } from '../../models/types';

type Props = {
  room: TypeRoom;
  statusGame: StatusGame;
  currentUserName: string;
  timer: number | null;
  gameTimer: number | null;
  textEditor: TextEditor;
  leaveRoom: (id: string) => void;
  toggleReadyUser: () => void;
};

function Room({
  room,
  statusGame,
  currentUserName,
  timer,
  gameTimer,
  textEditor,
  leaveRoom,
  toggleReadyUser,
}: Props): JSX.Element {
  const { typedText, currentChar, targetText } = textEditor;
  const { isWait, isTimer, isGame } = statusGame;
  const currentUser = room.users.find((e) => e.name === currentUserName) as TypeUser;

  const leave = () => {
    leaveRoom(room.id);
  };

  const users = room.users.map((user) => (
    <UserInfo key={user.id} user={user} current={user.name === currentUserName} />
  ));

  return (
    <Segment basic size="massive" className="room-page">
      <div className="room-aside">
        <Segment basic className="header-wrap">
          <Header as="h1">{room.name}</Header>
          {isWait && (
            <Button size="massive" className="button-back" onClick={leave}>
              <Icon name="chevron left" />
              Back To Rooms
            </Button>
          )}
        </Segment>
        <Segment basic className="users-info">
          {users}
        </Segment>
      </div>
      <Segment size="massive" className="room-main">
        {isWait && (
          <Button
            size="massive"
            className="button-user-status"
            color={currentUser.isReady ? 'red' : 'green'}
            onClick={toggleReadyUser}
          >
            {currentUser.isReady ? 'Not ready' : 'Ready'}
          </Button>
        )}
        {isTimer && <div className="room-counter">{timer}</div>}
        {isGame && (
          <div className="game-text">
            <span className="typed-text">{typedText}</span>
            <span className="current-char">{currentChar}</span>
            <span className="target-text">{targetText}</span>
          </div>
        )}
        {gameTimer && <div className="game-timer">{gameTimer} seconds left</div>}
      </Segment>
    </Segment>
  );
}

export default Room;
