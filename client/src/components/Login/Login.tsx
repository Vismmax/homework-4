import React, { useState } from 'react';
import { Form } from 'semantic-ui-react';
import './Login.css';

type Props = {
  login: (name: string) => void;
};

function Login({ login }: Props): JSX.Element {
  const [name, setName] = useState('');

  const onChangeHandler = (ev: React.ChangeEvent<HTMLInputElement>): void => {
    setName(ev.target.value);
  };

  const onSubmitHandler = (ev: React.FormEvent<HTMLFormElement>): void => {
    ev.preventDefault();
    if (!name) {
      return;
    }
    login(name);
  };

  return (
    <div className="login-page full-screen flex-centered">
      <Form className="flex" onSubmit={onSubmitHandler}>
        <Form.Group>
          <Form.Input autoFocus placeholder="username" type="text" size="big" value={name} onChange={onChangeHandler} />
          <Form.Button className="submit-button" type="submit" size="big" disabled={!name}>
            submit
          </Form.Button>
        </Form.Group>
      </Form>
    </div>
  );
}

export default Login;
