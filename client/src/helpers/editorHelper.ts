import { TextEditor } from '../models/types';

export default class EditorHelper {
  private static instance: EditorHelper;
  private state: TextEditor;

  private constructor() {
    this.state = {
      text: '',
      typedText: '',
      targetText: '',
      currentChar: '',
    };
  }

  public static getInstance(): EditorHelper {
    if (!EditorHelper.instance) {
      EditorHelper.instance = new EditorHelper();
    }

    return EditorHelper.instance;
  }

  public setState(props: {}) {
    this.state = {
      ...this.state,
      ...props,
    };
    return this.state;
  }

  public getState() {
    return this.state;
  }

  public progress() {
    return ((this.state.typedText as string).length * 100) / this.state.text.length;
  }
}
