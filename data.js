export const texts = [
  'Text for typing #1',
  'Text for typing #2',
  'Text for typing #3',
  'Text for typing #4',
  'Text for typing #5',
  'Text for typing #6',
  'Text for typing #7',
];

export const jokes = [
  'Про нашего последнего участника могу рассказать анекдот: Во время гонки на этапе формулы 1 армянский гонщик вдруг резко затормозил у обочины, подобрал пассажиров и поехал дальше. Судя по скорости, все именно так и происходит.',
  'И было у старика Шумахера три сына: двое гонщиков, а третий — водитель маршрутки.',
  '— Да—а, гонки уже подходят к концу, а российский гонщик все никак не догоняет! Не догоняет, что надо ехать в другую сторону!',
  'Мыкола Кулебяка из г. Синие Петухи назван лучшим в мире гонщиком. Он гонит самогон, который валит всех подряд уже после первых ста граммов.',
  'Вчера лежачий полицейский догнал эстонского гонщика.',
  'На трассе "Клавогонок" наш гонщик показал семнадцатое время, отстав от победителя всего на сутки.',
  'Виталий Петров - единственный гонщик "Клавогонок", который при виде полиции мигает фарами другим гонщикам.',
];

export default { texts, jokes };
